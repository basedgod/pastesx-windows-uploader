﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using Microsoft.Win32;


namespace pastesx_context_menu
{
    internal static class Program
    {
        [STAThread]
        private static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                try
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                              "\\PasteSx\\");
                    File.Copy(Application.ExecutablePath,
                        Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                        "\\PasteSx\\contextmenu.exe", true);
                    Registry.SetValue("HKEY_CLASSES_ROOT\\*\\shell\\Upload to PasteSx\\command", "",
                        "\"" + Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                        "\\PasteSx\\contextmenu.exe\" \"%1\"");
                    MessageBox.Show("Right click the file you want to upload!");
                    return 0;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            if (Properties.Settings.Default.APIKey.Equals("")) ErrorHandler("emptyApi");
            var apiKey = Properties.Settings.Default.APIKey;
            var contentReader = new StreamReader(args[0]);
            var content = contentReader.ReadToEnd();
            byte[] pasteWebClientResponseBytes;
            var pastePosterWebClient = new WebClient();
            try
            {
                pasteWebClientResponseBytes = pastePosterWebClient.UploadValues("https://paste.sx/api.php",
                    new NameValueCollection()
                    {
                        {"api_key", apiKey},
                        {"title", Path.GetFileName(args[0]) + " | Uploaded with PasteSx for Windows"},
                        {"content", content},
                        {"password", ""},
                        {"encrypted", "0"}
                    });
            }
            catch (WebException exception)
            {
                ErrorHandler(exception.Message + "\nStatus:" + exception.Status);
                return 0;
            }
            var responseSerializer = new JavaScriptSerializer();
            var responseDeserialized = responseSerializer.Deserialize<Response>(Encoding.UTF8.GetString(pasteWebClientResponseBytes));
            if (responseDeserialized.Status.Contains("Error"))
            {
                ErrorHandler(responseDeserialized);
            }
            else
            {
                MessageBox.Show("Paste uploaded successfully!\nLink has been copied into your clipboard!");
                System.Diagnostics.Process.Start(responseDeserialized.Link);
                Clipboard.SetText(responseDeserialized.Link);
                GC.Collect();
                return 1;

            }
            return 0;
        }

        private static void ShowSetApiKeyForm(bool showError = false)
        {
            if(showError)
                MessageBox.Show("Error your API Key is not set", "API Key Not Set!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            var formSetApiKey = new SetApiKey();
            formSetApiKey.ShowDialog();
        }
        private static void ErrorHandler(Response response)
        {
            switch (response.Message)
            {
                case "API Key not set.":
                    ShowSetApiKeyForm(true);
                    MessageBox.Show("Try your paste again!", "Retry paste!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case "Invalid API key format.":
                    MessageBox.Show("Error your API Key is invalid", "API Key Not Set!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ShowSetApiKeyForm();
                    MessageBox.Show("Try your paste again!", "Retry paste!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                default:
                    MessageBox.Show("Something went wrong while uploading your paste!\nError: " + response.Message);
                    break;
            }

        }
        private static void ErrorHandler(string error)
        {
            switch (error)
            {
                case "emptyApi":
                    ShowSetApiKeyForm(true);

                    break;
                default:
                    MessageBox.Show("Something went wrong while uploading your paste!\nError: " + error);
                    break;

            }

        }

    }
}

