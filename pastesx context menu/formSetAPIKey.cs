﻿using System;

using System.Windows.Forms;

namespace pastesx_context_menu
{
    public partial class SetApiKey : Form
    {
        public SetApiKey()
        {
            InitializeComponent();
        }

        private void formSettings_Load(object sender, EventArgs e)
        {
            textApiKey.Text = Properties.Settings.Default.APIKey;
        }

        private void buttonSetAPIKey_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.APIKey = textApiKey.Text;
            Properties.Settings.Default.Save();
            Close();
        }
    }
}
