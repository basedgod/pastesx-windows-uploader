﻿namespace pastesx_context_menu
{
    partial class SetApiKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAPIKey = new System.Windows.Forms.Label();
            this.textApiKey = new System.Windows.Forms.TextBox();
            this.buttonSetAPIKey = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelAPIKey
            // 
            this.labelAPIKey.AutoSize = true;
            this.labelAPIKey.Location = new System.Drawing.Point(14, 16);
            this.labelAPIKey.Name = "labelAPIKey";
            this.labelAPIKey.Size = new System.Drawing.Size(48, 13);
            this.labelAPIKey.TabIndex = 0;
            this.labelAPIKey.Text = "API Key:";
            // 
            // textApiKey
            // 
            this.textApiKey.Location = new System.Drawing.Point(68, 13);
            this.textApiKey.Name = "textApiKey";
            this.textApiKey.Size = new System.Drawing.Size(366, 20);
            this.textApiKey.TabIndex = 1;
            // 
            // buttonSetAPIKey
            // 
            this.buttonSetAPIKey.Location = new System.Drawing.Point(17, 39);
            this.buttonSetAPIKey.Name = "buttonSetAPIKey";
            this.buttonSetAPIKey.Size = new System.Drawing.Size(417, 26);
            this.buttonSetAPIKey.TabIndex = 2;
            this.buttonSetAPIKey.Text = "Set API Key";
            this.buttonSetAPIKey.UseVisualStyleBackColor = true;
            this.buttonSetAPIKey.Click += new System.EventHandler(this.buttonSetAPIKey_Click);
            // 
            // SetApiKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 81);
            this.Controls.Add(this.buttonSetAPIKey);
            this.Controls.Add(this.textApiKey);
            this.Controls.Add(this.labelAPIKey);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SetApiKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set API Key";
            this.Load += new System.EventHandler(this.formSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAPIKey;
        private System.Windows.Forms.TextBox textApiKey;
        private System.Windows.Forms.Button buttonSetAPIKey;
    }
}