﻿
namespace pastesx_context_menu
{
    public class Response
    {
        public string Status  { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
    }
}
